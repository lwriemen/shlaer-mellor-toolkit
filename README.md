Shlaer-Mellor Toolkit

Shlaer-Mellor models for Shlaer-Mellor development tools with the ability to: graphically create Shlaer-Mellor models, verify the models prior to translation, translate the models into a lower-level software language, configuration management of the models, platform-specific project creation and management, and automated document creation. Each subject matter will be contained in it's own (sub)project.

[The Shlaer-Mellor Toolkit blog](http://shlaermellortoolkit.blogspot.com)

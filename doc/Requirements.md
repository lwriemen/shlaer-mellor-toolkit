# Introduction #

The requirements are based on the ideal toolkit, and should be further detailed in the individual domain projects.

# Toolkit Requirements #
Required elements

  1. Graphically create Shlaer-Mellor models.
  1. Graphical verification of models; not necessarily animated.
  1. Platform (includes 3GL) independent action language.
  1. Support for model documentation. i.e., each model element should have at the very least an associated textual description. (Build into meta model?)
  1. Automated model document generation.
  1. Translation (model compiler) support.
  1. Project configuration support. i.e., ability to combine domains, implemented bridges, translation marks/coloring, and tests into a single project configuration.

# Toolkit Wants #
Desirable elements

  1. Automatic metrics support
  1. Requirements tracing

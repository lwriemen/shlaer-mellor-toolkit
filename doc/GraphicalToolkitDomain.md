# Mission Statement #

The graphical toolkit domain provides drawing elements to construct UML models. This should be a realized domain.

# Bridges #

| **Bridge To** | **Domain Assumptions on Bridge** |
|:--------------|:---------------------------------|

# Domain Chart #

Insert domain chart here.

# Domain Objects #
# Mission Statement #

The metamodel domain defines the available notations, constraints, and completeness rules for creating all Shlaer-Mellor models.

# Bridges #

The metamodel domain doesn't require services from any other domain.

# Domain Chart #

As the metamodel domain is wholly self contained, i.e., it doesn't require services from any other domain., there is no need to draw a domain chart around it.

# Domain Objects #

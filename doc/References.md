# References #

## OOSA ##
  * **Object-Oriented Systems Analysis: Modeling the World in Data**. Sally Shlaer, Stephen Mellor. Yourdon Press. 1988. ISBN 013629023X.
## OLMWS ##
  * **Object Lifecycles: Modeling the World in States**. Sally Shlaer, Stephen Mellor. Yourdon Press. 1991. ISBN 0136299407.
## Mellor-Balcer ##
  * **Executable UML: A Foundation for Model-Driven Architecture**. Mellor, Stephen J.; Balcer, Marc. Addison Wesley. 2002. ISBN 0201748045.
## xUML ##
  * **Model-Driven Architecture with Executable UML**. Raistrick, Chris; Francis, Paul; Wright, John; Carter, Colin; Wilkie, Ian. Cambridge University Press. 2004. ISBN 0521537711.
## Starr-SM ##
  * **How to Build Shlaer-Mellor Object Models**. Leon Starr. Prentice Hall. 1996. ISBN 0132076632.
## Starr ##
  * **Executable UML: How to Build Class Models**. Starr, Leon. Prentice-Hall. 2002. ISBN 0130674796.
## Lahman ##
  * **Model-Based Development: Applications**. Lahman, H. S. Addison-Wesley Professional. 2011. ISBN 0321774078.
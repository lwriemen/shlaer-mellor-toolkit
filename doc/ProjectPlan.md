# Summary #
This page provides goals and direction on project development.

# Project Management #
Project management ideas and advice to aid in project startup and planning.
## Estimates ##
Metrics are sorely lacking in the software world. It would be highly desirable to have initial estimates on each domain before modeling begins. The estimates should be done using IFPUG Function Points according to the guidelines at [Function Point Analysis](FunctionPointAnalysis.md).

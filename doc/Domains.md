# Domains #
Shlaer-Mellor development begins with partitioning the system into areas of subject matter that are abstracted as domains. Each domain is considered a wholly encapsulated software system, therefore each of the domains identified for this project will be developed as individual projects. Domains are also substitutable, so multiple projects can be spawned from a single domain definition.

Development of identified domains involves starting a new project and updating the link on this page to point to the project. Sample links point to a page that was created using the [domain template](DomainTemplate.md). Individual domain projects should be added to the table to be easily found under "Project link"; if multiple projects exist for a domain, then extra "Project link" headings can be added.

## Identified Domains ##
| **Domain Name** | **Sample page** | **Project link** |
|:----------------|:----------------|:-----------------|
| Metamodel | [MetamodelDomain](MetamodelDomain.md) | [shlaer-mellor-metamodel](https://codeberg.org/lwriemen/shlaer-mellor-metamodel/) |
| Model Compiler | [ModelCompilerDomain](ModelCompilerDomain.md) |  |
| Model Verifier | [ModelVerifierDomain](ModelVerifierDomain.md) |  |
| Modeling | [GraphicalModelingDomain](GraphicalModelingDomain.md) |  |
| Toolkit | [GraphicalToolkitDomain](GraphicalToolkitDomain.md) |  |
| Project Management | [ProjectDomain](ProjectDomain.md) |  |

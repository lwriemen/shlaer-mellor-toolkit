# Introduction #

These guidelines assume IFPUG Function Points and were gleaned from the object-oriented section of the Garmus and Herron FPA book.

These guidelines have not been rigorously tested!

Read this [paper](http://www.frogooa.com/downloads/papers/Function%20Point%20Analysis%20and%20Executable%20UML.pdf) for details.
# Mission Statement #

Per [Mellor-Balcer](References#Mellor-Balcer.md), "an Executable UML model compiler turns an executable UML model into an implementation using a set of decisions about the target hardware and software environment." (Executable UML in this context is equal to Shlaer-Mellor.)

Individual model compiler domains should be explicit about the target environment in their mission statements.

# Bridges #

| **Bridge To** | **Domain Assumptions on Bridge** |
|:--------------|:---------------------------------|
| [MetamodelDomain](MetamodelDomain.md) |  |

There'll definitely be bridges to implementation domains listed here.

# Domain Chart #

# Domain Objects #

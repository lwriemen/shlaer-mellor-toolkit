# Mission Statement #

Add your mission statement here.
  * From [Object Lifecycles](References#OLMWS.md), "The mission statement should provide a clear charter for constructing the OOA models for the domain."
  * Table of examples from [Mellor-Balcer](References#Mellor-Balcer.md):
| **Domain** | **Mission Statement** |
|:-----------|:----------------------|
| On-line Bookstore | Provides a way for customers to place orders for books and other goods. |
| Web GUI | Provides a way for on-line users to interact with a system. |
| Messaging | Provides a way to communicate information between the application and other independent software systems. |
| HTML | Provides a way to render user interface displays as pages that can be displayed in a web browser, and for user input to be captured in web forms and transmitted back to the application. |


# Bridges #

List bridges to other domains here. (You can use the table below with the header intact. The example fill row is from [Mellor-Balcer](References#Mellor-Balcer.md). The domain being modeled is the Web GUI.
| **Bridge To** | **Domain Assumptions on Bridge** |
|:--------------|:---------------------------------|
| Web Server | All display communication will be over secure communication. |

# Domain Chart #

Insert domain chart here.
  * Application domains should link to generic service domains, if it makes sense, for a wider range of substitution. In the examples above, an on-line bookstore would only ever use a Web GUI, so a generic GUI wouldn't make sense.
  * Service domains can link to a generic "Application" domain.
  * Architecture and implementation domains should either be omitted or left as generic as possible.

# Domain Objects #

A good place to put the output of the "object blitz", but this section will probably not have much utility when the domain has been modeled. It's purpose is mainly for planning.
# Mission Statement #

The project domain is concerned with packaging platform-independent models with platform-specific concerns. Domains (modeled and realized), bridge implementations, model compiler marks, test results and platform-specific documentation are all items that should be encapsulated for project release.

# Bridges #

| **Bridge To** | **Domain Assumptions on Bridge** |
|:--------------|:---------------------------------|

# Domain Chart #

Insert domain chart here.

# Domain Objects #
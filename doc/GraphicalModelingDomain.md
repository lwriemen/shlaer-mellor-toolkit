# Mission Statement #

The graphical modeling domain provides for construction of Shlaer-Mellor models per the metamodel rules.

# Bridges #

| **Bridge To** | **Domain Assumptions on Bridge** |
|:--------------|:---------------------------------|
| [Metamodel](MetamodelDomain.md) |  |

# Domain Chart #

Insert domain chart here.

# Domain Objects #

# About #

The Shlaer-Mellor Toolkit is intended to help make Shlaer-Mellor development more attractive to the hobbyist and the newcomer by making it easier to create Shlaer-Mellor tools.

The goal is to produce Shlaer-Mellor models for the different subject matter aspects of Shlaer-Mellor development for use in software projects.

For learning about , see the [Wikipedia entry](https://en.wikipedia.org/wiki/Executable_UML).

# Reasoning #

At the start of this planning effort, the commercial Shlaer-Mellor tools were too expensive for the hobbyist; the cost was often a roadblock to adoption by newcomers.

Shlaer-Mellor can be done in UML, but generic UML tools are cumbersome for experienced Shlaer-Mellor modelers. They also offer too many non-Shlaer-Mellor elements for newcomers to trip over.

In current times, [BridgePoint](https://xtuml.org/) has gone free and open source, but it has also gone to strictly UML notation. [Model Integration](https://modelint.com/) and Model Realization have developed tools, but they are more text based and not as automated as BridgePoint.

While the  literature does a very good job of describing the method, there has never been a clear view of actual usage in projects and all reuse scenarios. Some of this is being addressed by the [Shlaer-Mellor Commons](https://www.shlaermellorcommons.org)


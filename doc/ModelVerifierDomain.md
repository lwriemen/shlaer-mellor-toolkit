# Mission Statement #

The model verifier domain allows platform independent execution of the domain model for test purposes.

# Bridges #

| **Bridge To** | **Domain Assumptions on Bridge** |
|:--------------|:---------------------------------|
| [Metamodel](MetamodelDomain.md) |  |

# Domain Chart #

Insert domain chart here.

# Domain Objects #